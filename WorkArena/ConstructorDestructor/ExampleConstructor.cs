﻿using System;

namespace WorkArena.ConstructorDestructor
{
    class ExampleConstructor
    {
        public ExampleConstructor()
        {
            int[][] arr = { new[] { 1, 2, 3 }, new[] { 4, 5, 6 }, new[] { 7, 8 } };
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    Console.Write(arr[i][j] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }

        ~ExampleConstructor()
        {
            Console.WriteLine("destructor !!");
        }
    }
}
