﻿using System;

namespace WorkArena.Aggregation
{
    class EmployeeAddress
    {
        public int Id;
        public string Name;
        public Address EmployeeAdd;

        public EmployeeAddress(int id, string name, Address employeeAdd)
        {
            this.Id = id;
            this.Name = name;
            this.EmployeeAdd = employeeAdd;
        }

        public void Display()
        {
            Console.WriteLine(Id+" "+Name+" "+EmployeeAdd.AddressLine+" "+EmployeeAdd.City+" "+EmployeeAdd.State);
        }
    }
}
