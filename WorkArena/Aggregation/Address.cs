﻿namespace WorkArena.Aggregation
{
    class Address
    {
        public string AddressLine, City, State;
        public Address(string addressLine, string city, string state)
        {
            this.AddressLine = addressLine;
            this.City = city;
            this.State = state;
        }
    }
}
