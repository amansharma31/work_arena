﻿using System;
using System.Text;

namespace WorkArena.ArrayString
{
    class StringBuiltInMethods
    {
        string MyString = "  Thumbmunkeys pvt ltd is a UK based Company";

        public void StringMethods()
        {
            //MyString = MyString.Substring(6, 16);
            //MyString = MyString.ToUpper();
            //MyString = MyString.Replace("a", "z");
            //MyString = MyString.Remove(0, 12);
            int length = MyString.Length;
            string name = MyString.Trim(char.Parse(" "));
            MyString = MyString.Replace(" ", String.Empty);
            int newLength = name.Length;
            Console.WriteLine(MyString);
            Console.WriteLine("length is : {0}", length);
            Console.WriteLine("New length is: {0}", newLength);
            StringBuilder myString = new StringBuilder();
            for (int i = 0; i < 10; i++)
            {
                myString.Append("+");
                myString.Append(i);
            }
            Console.WriteLine(myString);
            Console.ReadLine();
        }
    }
}
