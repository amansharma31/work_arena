﻿using System;

namespace WorkArena.Inheritance
{
    class Dog: Animal
    {
        public void Bark()
        {
            Console.WriteLine("Dog barks..");
        }
        public void Run()
        {
            Console.WriteLine("Dog runs fastly!!");
        }
    }
}
