﻿using System;

namespace WorkArena.Inheritance
{
    class Bike
    {
        public int Time=2;
        public int Distance = 90;
        public void CalculateSpeed()
        {
            float speed = Distance / Time;
            Console.WriteLine("Speed of bike is : {0}km/hr",speed);
        }

        public void BikeType()
        {
            Console.WriteLine("New model is honda !!");
        }
    }
}
