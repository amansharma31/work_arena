﻿
using System;
using WorkArena.Aggregation;
using WorkArena.ArrayString;
using WorkArena.ConstructorDestructor;
using WorkArena.DateAndTime;
using WorkArena.Enums;
using WorkArena.Inheritance;
using WorkArena.PropertiesUse;
using WorkArena.StaticAndThis;
using WorkArena.Structs;
using WorkArena.WhileForUse;

namespace WorkArena
{
    class ClassMain
    {
        public static object PropertyUsage { get; private set; }

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter 1 to do array and string stuff:\n" +
                                  "Enter 2 to do Constructor Stuff:\n" +
                                  "Enter 3 to do WhileFor Use:\n" +
                                  "Enter 4 to see use of This keyword:\n" +
                                  "Enter 5 to see Static keyword use:\n" +
                                  "Enter 6 to See Structs use:\n" +
                                  "Enter 7 to See Enum Implementation:\n" +
                                  "Enter 8 to see Date and time program:\n" +
                                  "Enter 9 to do Inheritance:\n" +
                                  "Enter 10 to do Aggregation:\n" +
                                  "Enter 11 to see properties Usage");

                int n = int.Parse(Console.ReadLine());

                switch (n)
                {
                    case 1:
                        {
                            int[] arr = { 1, 2, 3, 4, 5 };
                            ExampleArray array = new ExampleArray();
                            array.PrintArray();
                            array.PrintString();
                            array.ReverseString();
                            array.StringReverse();
                            ArrayToFunction arrayMaximum = new ArrayToFunction();
                            arrayMaximum.ArrayMaxElement(arr);
                            StringBuiltInMethods methods = new StringBuiltInMethods();
                            methods.StringMethods();
                        }
                        break;

                    case 2:
                        {
                            ExampleConstructor constructor = new ExampleConstructor();
                        }
                        break;

                    case 3:
                        {
                            WhileForUsage useWhile = new WhileForUsage();
                            useWhile.Display();
                        }
                        break;
                    case 4:
                        {
                            var callThisUse = new ThisUsage(22, "thumbmunkeys", 8.6f);
                            callThisUse.DisplayFields();
                        }
                        break;
                    case 5:
                        {
                            Account.RateOfInterest = 7.8f;
                            Account getDetails = new Account();
                            getDetails.AccountDetails(1234556, "Aman");
                            getDetails.DisplayAccountDetails();
                        }
                        break;
                    case 6:
                        {
                            Circle circleStructs = new Circle();
                            circleStructs.CircleDetails(4, 3.14f);
                            circleStructs.AreaOfCircle();
                        }
                        break;
                    case 7:
                        {
                            EnumUsage getEnum = new EnumUsage();
                            getEnum.EnumValues();
                        }
                        break;
                    case 8:
                        {
                            DateAndTimesProgram dateMethod = new DateAndTimesProgram();
                            dateMethod.ShowDateAndTime();
                        }
                        break;
                    case 9:
                        {
                            Dog animal = new Dog();
                            animal.Bark();
                            animal.Eat();
                            animal.Run();
                            animal.Speak();

                            HeroHonda bike = new HeroHonda();
                            bike.CalculateSpeed();
                            bike.BikeModelName();
                            bike.BikeType();
                            Console.WriteLine();
                        }
                        break;
                    case 10:
                        {
                            Address getAddress = new Address("Tareen bahadur Ganj", "Noida", "UP");
                            EmployeeAddress getEmployeeAdd = new EmployeeAddress(1, "Aman", getAddress);
                            getEmployeeAdd.Display();
                            Console.WriteLine();

                        }
                        break;
                    case 11:
                        {
                            PropertiesUsage property = new PropertiesUsage("Aman",22,12.2f);
                            property.DisplayProperties();
                            PropertyUse propertyUse=new PropertyUse("Aman",22);
                            propertyUse.DisplayValues();

                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Wrong choice entered!!");
                        }
                        break;
                }
            }
        }
    }
}
