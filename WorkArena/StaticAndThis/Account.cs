﻿using System;

namespace WorkArena.StaticAndThis
{
    class Account
    {
        public int AccountNumber;
        public string Name;
        public static float RateOfInterest;

        public void AccountDetails(int accountNumber, string name)
        {
            AccountNumber = accountNumber;
            Name = name;
        }

        static Account()
        {
            RateOfInterest = 9.5f;
        }

        public void DisplayAccountDetails()
        {
            Console.WriteLine(Name + " " + AccountNumber + " " + RateOfInterest);
            Console.ReadLine();
        }
    }
}
