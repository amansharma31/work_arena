﻿using System;
using System.CodeDom;

namespace WorkArena.StaticAndThis
{
    class ThisUsage
    {
        public int Age;
        public string Name;
        public float Cgpa;

        public ThisUsage(int age, string name,float cgpa)
        {
            this.Age = age;
            this.Name = name;
            this.Cgpa = cgpa;
        }

        public void DisplayFields()
        {
            Console.WriteLine("age is: name is: cgpa is: {0},{1},{2}",Age,Name,Cgpa);
            Console.ReadLine();
        }
        
    }
}
