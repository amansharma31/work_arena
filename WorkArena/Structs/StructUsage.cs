﻿using System;

namespace WorkArena.Structs
{
    public struct Circle
    {
        public int Radius;
        public float Pi;

        public void CircleDetails(int radius, float pi)
        {
            Radius = radius;
            Pi = pi;
        }

        public void AreaOfCircle()
        {
            Console.WriteLine("Area is ; {0}", Pi * Radius * Radius);
            Console.ReadLine();
        }
    }
}
